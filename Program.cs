﻿using System;
using System.Collections.Generic;

namespace _5002_ASSN3_GR02
{
    class Program
    {
        static void Main(string[] args)
        {
            // Vars
            bool       loop        = true;         // Used for program main loop
            double     res         = 0;            // Used to determine which case is executed in switch

            // Main loop
            while (loop)
            {
                // Present user with menu
                PresentMenu();
                
                // Check that the user has entered a number
                if (double.TryParse(Console.ReadLine(), out res))
                {
                    switch (Convert.ToInt32(res))
                    {
                        case 1:
                            Console.Clear();
                            Console.WriteLine($"You have selected option {Convert.ToInt32(res)}.");

                            bool    finished    = false;
                            int     rVal        = 0;

                            while (!finished)
                            {
                                Console.Write("Options: \n1 - Check if a year is a leap year. \n2 - Find the next leap year after a specified year. \nAny other key - Return to the menu.\n");

                                if (!int.TryParse(Console.ReadLine(), out rVal) || rVal > 2 || rVal < 1) break; // Only proceed if the user entered 1 || 2

                                if (rVal == 1) Console.WriteLine(TaskOne());
                                else 
                                {
                                    Console.WriteLine("\nEnter a year.");
                                    // What is printed to the screen = Did user enter a number ? TaskOne(year) : Error message
                                    Console.WriteLine(int.TryParse(Console.ReadLine(), out rVal) == true ? TaskOne(rVal) : "You did not enter a year, returning to options.\n");
                                }
                            }
                            break;

                        case 2:
                            Console.WriteLine($"You have selected option {Convert.ToInt32(res)}.");
                            TaskTwo();
                            break;

                        case 3:
                            Console.WriteLine($"You have selected option {Convert.ToInt32(res)}.");
                            TaskThree();
                            break;

                        case 4:
                            Console.WriteLine($"You have selected option {Convert.ToInt32(res)}.");

                            // Get input from user
                            Console.WriteLine("\nPlease type in a number");
                            var input = Console.ReadLine();
                            Task4Class sumClass = new Task4Class(ref input);
                            Console.WriteLine(input);
                            break;

                        case 5:
                            loop = false;
                            continue;

                        default:
                            Console.WriteLine("Please enter a number between 1 and 5.");
                            break;
                    }
                    Console.WriteLine("Press <ENTER> to return to the menu.");
                    Console.ReadKey();
                }
                else        // The user did not input a number
                {
                    Console.WriteLine("You did not enter a number, press <ENTER> to try again.");
                    Console.ReadKey();
                }
            }

            // End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Thankyou for using our awesome program.");
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }

        static public void PresentMenu()
        {
            Console.Clear();
            Console.WriteLine("***********************************************************");
            Console.WriteLine("****     WELCOME TO THE MOST AMAZING PROGRAM EVER      ****");
            Console.WriteLine("***********************************************************");
            Console.WriteLine("**** 1: Amazing leap year tools                        ****");
            Console.WriteLine("**** 2: Such awesome even random numbers               ****");
            Console.WriteLine("**** 3: Very ascending & decending words               ****");
            Console.WriteLine("**** 4: Summing natural numbers, go get em tiger       ****");
            Console.WriteLine("**** 5: Exit program =[                                ****");
            Console.WriteLine("***********************************************************\n");
            Console.WriteLine("Pease enter a number that matches a task.\n");
        }

        static public string TaskOne()
        {
            Console.WriteLine("\nPlease enter a year number, eg. 1988.");
            int year = int.Parse(Console.ReadLine());
            return year % 4 == 0 ? $"\n{year} is a leap year \n" : $"\n{year} is not a leap year \n";
        }
        // Overload method to find the next leap year after a year specified by the user
        static public string TaskOne(int year)
        {
            int nextLeapYear = year;
            for (int i = 0; i < 4; i++) {
                if (++nextLeapYear % 4 == 0) break;
            }
            return $"\nThe first leap year after {year} is {nextLeapYear}.\n";
        }

        static public void TaskTwo()
        {
            int CurrentNumber;
            List<int> Numbers = new List<int>();
            Random Rand = new Random();
            for(int i = 0; i < 100; i++)
            {
                CurrentNumber = Rand.Next();
                Numbers.Add(CurrentNumber);
            }
            foreach(int Number in Numbers)
            {
                if(Number % 2 == 0)
                {
                    Console.WriteLine(Number);
                }
            }
        }

        static public void TaskThree()
        {
            // Create 100 word Array.
            var words = new string[] {"Emlyn","Nisei","Toyon","Laith","Aloof","Third","Coset","Daffy","Barca","Poice",
            "Sedan","Opium","Verdi","Cecal","Mtif","Herse","Skete","Birth","Kitwe","Levee",
            "Niort","Suety","Fiord","Oisin","Nikon","Lindy","Cuppy","Hotiy","Mucid","Purin",
            "Radii","Fichu","Desai","Bluey","Ansel","Nixes","Growl","CLonk","Pilar","Dingy",
            "Hench","Easer","Firer","Loamy","Basle","Janis","Filth","Ergot","Balac","Witta",
            "Hague","Crook","Dooms","Aeaea","Brice","Toque","Venom","Dinar","Poeas","Marla",
            "Lumpy","Boole","Limbs","Nomad","Flies","Vexed","Liang","Neddy","Dummy","Laded",
            "Druid","Hazen","Tulle","Jewel","Foist","Renan","Roose","Refer","Adele","Rummy",
            "Mabel","Latex","Tanna","Adair","Dakar","Mossy","Warty","Carey","Fatty","Dogie",
            "Olden","Vivid","Watap","Mucor","Nllst","Harns","Meany","Balch","Strop","Tibia"
            };
            
            // Sort Array in alphabetical order.
            Array.Sort(words);

            // Print Array on screen in ascending order.
            Console.WriteLine("\nWords in ascending order. \n");
            Console.WriteLine(string.Join("\n", words));
            Console.Write("\n");
            
            // Reversing the Array (Decending order).
            Array.Reverse(words);
        
            // Print Array on screen in descending order.
            Console.WriteLine("Words in descending order. \n");
            Console.WriteLine(string.Join("\n", words));

            Console.Write("\n");
        }
    }
}
