public class Task4Class
{
    public Task4Class(ref string input)
    {
        //Double check that the number is infact a number
        var c = 0;
        if (int.TryParse(input, out c))
        {
            //pass in that number's variable - (c) - as part of your loop
            int a, b, i, Sum = 0;
            for (i = 0; i < c; i++)
            {
                a = i % 3;
                b = i % 5;
                if (a == 0 || b == 0)
                {
                    //Console.Write("{0}\t", i);
                    Sum = Sum + i;
                }
            }

            input = $"\nThe Sum of all the Multiples of 3 or 5 Below 1000 : {Sum}";
        }
        else
        {
            input = "This isn't a number";                        
        }
    }
}